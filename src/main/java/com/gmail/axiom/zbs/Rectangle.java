package com.gmail.axiom.zbs;

public class Rectangle extends Shape {
    private Point a;
    private Point b;
    private Point c;
    private Point d;

    public Rectangle(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4) {
        this.a = new Point(x1, y1);
        this.b = new Point(x2, y2);
        this.c = new Point(x3, y3);
        this.d = new Point(x4, y4);
    }

    public Rectangle(Point a, Point b, Point c, Point d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public Rectangle(Rectangle rectangle) {
        this.a = rectangle.a;
        this.b = rectangle.b;
        this.c = rectangle.c;
        this.d = rectangle.d;
    }

    public double getLengthOfAFirstSide() {
        return Math.abs(Math.sqrt(Math.pow(b.getX() - a.getX(), 2) + Math.pow(b.getY() - a.getY(), 2)));
    }

    public double getLengthOfASecondSide() {
        return Math.abs(Math.sqrt(Math.pow(c.getX() - b.getX(), 2) + Math.pow(c.getY() - b.getY(), 2)));
    }

    public double getPerimeter() {
        return (getLengthOfAFirstSide() + getLengthOfASecondSide()) * 2;
    }

    public double getArea() {
        return getLengthOfAFirstSide() * getLengthOfASecondSide();
    }
}

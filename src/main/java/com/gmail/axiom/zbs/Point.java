package com.gmail.axiom.zbs;

public class Point {
    private double x = 0;
    private double y = 0;

    public Point() {
        this.x = x;
        this.y = y;
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point(Point a) {
        this.x = a.x;
        this.y = a.y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setXY(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void shiftByTheVector(double x, double y) {
        this.x += x;
        this.y += y;
    }

    public void shiftLeft(double x) {
        this.x -= x;
    }

    public void shiftRight(double x) {
        this.x += x;
    }

    public void shiftUp(double y) {
        this.y += y;
    }

    public void shiftDown(double y) {
        this.y -= y;
    }
}

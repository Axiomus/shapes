package com.gmail.axiom.zbs;

public class Triangle extends Shape {
    private Point a;
    private Point b;
    private Point c;

    public Triangle(double x1, double y1, double x2, double y2, double x3, double y3) {
        this.a = new Point(x1, y1);
        this.b = new Point(x2, y2);
        this.c = new Point(x3, y3);
    }

    public Triangle(Point a, Point b, Point c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Triangle(Triangle triangle) {
        this.a = triangle.a;
        this.b = triangle.b;
        this.c = triangle.c;
    }

    public double getLengthOfAFirstSide() {
        return Math.abs(Math.sqrt(Math.pow(b.getX() - a.getX(), 2) + Math.pow(b.getY() - a.getY(), 2)));
    }

    public double getLengthOfASecondSide() {
        return Math.abs(Math.sqrt(Math.pow(c.getX() - b.getX(), 2) + Math.pow(c.getY() - b.getY(), 2)));
    }

    public double getLengthOfAThridSide() {
        return Math.abs(Math.sqrt(Math.pow(a.getX() - c.getX(), 2) + Math.pow(a.getY() - c.getY(), 2)));
    }

    public double getArea() {
        return Math.sqrt(getPerimeter() * (getPerimeter() - getLengthOfAFirstSide()) * (getPerimeter() - getLengthOfASecondSide()) * (getPerimeter() - getLengthOfAThridSide()));
    }

    public double getPerimeter() {
        return getLengthOfAFirstSide() + getLengthOfASecondSide() + getLengthOfAThridSide();
    }
}
